#include <iostream>
#include <openssl/sha.h>
#include <cstring>

using namespace std;

int main(int argc, char const *argv[])
{
    char data[1024] = "hello world!";
    int len = strlen(data);

    unsigned char md[SHA512_DIGEST_LENGTH] = {0};

    SHA512_CTX c;
    SHA512_Init(&c);
    SHA512_Update(&c, data, len);
    SHA512_Final(md, &c);
    // cout << md << endl;

    char buf[SHA512_DIGEST_LENGTH * 2 + 1] = {0};

    for (int i = 0; i < SHA512_DIGEST_LENGTH; i++)
    {
        sprintf(&buf[i * 2], "%02x", md[i]);
    }

    cout << buf << endl;

    return 0;
}
