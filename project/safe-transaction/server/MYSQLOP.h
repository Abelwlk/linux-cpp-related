#pragma once
#include <string>
#include <mysql/mysql.h>
#include "SecKeyShm.h"
using namespace std;

class MYSQLOP
{
private:
    MYSQL *mysql;
    MYSQL *conn;

private:
    // 获取当前时间, 并格式化为字符串
    string getCurTime();

public:
    MYSQLOP();
    ~MYSQLOP();

    // 初始化环境连接数据库
    bool connectDB(char *user, char *passwd, char *conntr);
    // 得到keyID
    int getKeyID();
    bool updataKeyID(int keyID);
    bool writeSecKey(NodeSHMInfo *pNode);
    void closeDB();
};
