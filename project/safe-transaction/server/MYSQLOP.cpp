#include "MYSQLOP.h"
#include <iostream>
#include <string.h>
#include <time.h>
using namespace std;

MYSQLOP::MYSQLOP() {}
MYSQLOP::~MYSQLOP() {}

bool MYSQLOP::connectDB(char *user, char *passwd, char *conntr)
{
    //初始化环境
    mysql = mysql_init(NULL);
    if (mysql == NULL)
    {
        printf("mysql init error\n");
        return false;
    }
    //创建连接
    conn = mysql_real_connect(mysql, NULL, user, passwd, conntr, 0, NULL, 0);
    if (conn == NULL)
    {
        printf("mysql_real_connect error,[%s]\n", mysql_error(mysql));
        return false;
    }
    printf("mysql connect ok!,[%p],[%p]\n", mysql, conn);
    if (!mysql_set_character_set(conn, "UTF8"))
    {
        printf("New client character set: %s\n", mysql_character_set_name(conn));
    }
    return true;
}

int MYSQLOP::getKeyID()
{
    //执行sql语句
    char sql[255] = "select ikeysn from keysn";
    int ret = mysql_query(conn, sql);
    if (ret != 0)
    {
        printf("mysql_query error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    //获取结果集
    MYSQL_RES *result = mysql_store_result(conn);
    int keyID = -1;
    //获取每一行记录
    MYSQL_ROW row = mysql_fetch_row(result);
    keyID = atoi(row[0]);
    //释放结果集
    mysql_free_result(result);
    return keyID;
}

// 秘钥ID在插入的时候回自动更新, 也可以手动更新
bool MYSQLOP::updataKeyID(int keyID)
{
    char sql[255] = "update keysn set ikeysn = ";
    sprintf(sql, "%s%d", sql, keyID);
    int ret = mysql_query(conn, sql);
    if (ret != 0)
    {
        printf("mysql_query error,[%s]\n", mysql_error(mysql));
        return false;
    }
    return true;
}
// 将生成的秘钥写入数据库
// 更新秘钥编号
bool MYSQLOP::writeSecKey(NodeSHMInfo *pNode)
{
    // 组织待插入的sql语句
    char sql[1024] = {0};
    sprintf(sql, "insert into seckeyinfo(clientid, serverid, keyid, createtime, state, seckey) values ('%s', '%s', %d, '%s' , %d, '%s') ",
            pNode->clientID, pNode->serverID, pNode->seckeyID, getCurTime().data(), 0, pNode->seckey);
    int ret = mysql_query(conn, sql);
    if (ret != 0)
    {
        printf("mysql_query error,[%s]\n", mysql_error(mysql));
        return false;
    }
    return true;
}

void MYSQLOP::closeDB()
{
    //关闭连接
    mysql_close(conn);
}

string MYSQLOP::getCurTime()
{
    time_t timep;
    time(&timep);
    char tmp[64];
    strftime(tmp, sizeof(tmp), "%Y-%m-%d %H:%M:%S", localtime(&timep));
    return tmp;
}