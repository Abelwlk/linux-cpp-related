#! /bin/bash
echo $0
echo $1
echo $2
echo $3

echo "参数个数:$#"
echo $@
echo "pid:$$"
echo $?

var1="hello"
var2="hello"
if [ $var1 = $var2 ]
then
    echo "yes"
else
    echo "no"
fi

n1=100
n2=100
if [ $n1 -gt $n2 ]
then
    echo "yes"
else
    echo "no"
fi

hour='date +%H'
echo "current time is $hour"
if [ $hour -gt 8 -a $hour -lt 12 ]
then
    echo "good morning"
else
    echo "good afternoon"
fi

if [ -f "a.txt" ]
then
    echo "a.txt is exist"
else
    echo "a.txt is not exist"
fi

for fruit in apple peer banana
do
    echo ${fruit}
done

for file in `ls`
do
echo $file
done

sum=0
for i in {1..100}
do
    sum=$[$sum+$i]
done
echo $sum

for file in `ls *.txt`
do
    mv $file $(basename $file .txt)
done
