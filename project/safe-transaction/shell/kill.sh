#! /bin/bash
PID=$(ps -ef | grep mysql_client | grep -v grep | awk -F " " '{print $2}')
echo $PID
if [ -n $PID ]
then
    kill -9 $PID
fi