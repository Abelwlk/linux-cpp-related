#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>

//信号处理函数
void sighandler(int signo)
{
    printf("signo = [%d]\n", signo);
}

int main(int argc, char const *argv[])
{
    int fd[2];
    int ret = pipe(fd);
    if (ret < 0)
    {
        perror("pipe error");
        return -1;
    }

    //注册信号处理函数
    signal(SIGPIPE, sighandler);

    close(fd[0]);
    //给没有读端的管道写数据，会产生SIGPIPE信号
    write(fd[1], "hello world!", strlen("hello world!"));

    return 0;
}
