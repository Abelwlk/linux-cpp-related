#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>

int main(int argc, char const *argv[])
{
    kill(getpid(), SIGKILL);
    printf("exec\n");
    return 0;
}
