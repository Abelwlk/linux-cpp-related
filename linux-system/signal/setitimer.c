#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/time.h>

void sighandler(int signo)
{
    printf("signo=[%d],hello world!\n", signo);
}
int main(int argc, char const *argv[])
{
    //注册信号处理函数
    signal(SIGALRM, sighandler);

    struct itimerval tm;

    //第一次触发时间赋值
    tm.it_value.tv_sec = 3;
    tm.it_value.tv_usec = 0;
    //周期性时间赋值
    tm.it_interval.tv_sec = 1;
    tm.it_interval.tv_usec = 0;

    setitimer(ITIMER_REAL, &tm, NULL);

    while (1)
    {
        sleep(1);
    }

    return 0;
}
