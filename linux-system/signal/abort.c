#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>

int main(int argc, char const *argv[])
{
    //给自己发送异常终止信号 6 sigabrt
    abort();
    return 0;
}