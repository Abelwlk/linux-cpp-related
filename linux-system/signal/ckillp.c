#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>

int main(int argc, char const *argv[])
{
    int i = 0;
    for (i = 0; i < 3; i++)
    {
        //创建子进程
        //pid_t fork(void)
        pid_t pid = fork();
        if (pid < 0)
        {
            perror("fork error");
            return -1;
        }
        else if (pid > 0)
        {
            //父进程
            printf("father:pid=[%d],fpid=[%d]\n", getpid(), getppid());
            if (i == 0)
            {
                //杀死第一个子进程
                //kill(pid, SIGKILL);
            }
        }
        else if (pid == 0)
        {
            //子进程
            printf("child:pid=[%d],fpid=[%d]\n", getpid(), getppid());
            //禁止子进程再创建孙子进程
            break;
        }
    }
    if (i == 0)
    {
        printf("child1:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        //子进程杀死父进程
        //kill(getppid(), SIGKILL);
        sleep(1);
        //杀死同一组的所有进程
        kill(0, SIGKILL);
    }
    if (i == 1)
    {
        printf("child2:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        sleep(10);
    }
    if (i == 2)
    {
        printf("child3:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        sleep(10);
    }
    if (i == 3)
    {
        printf("parent:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        sleep(10);
    }
    return 0;
}
