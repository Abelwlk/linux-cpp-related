#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/time.h>

//信号处理函数
void sighandler(int signo)
{
    printf("signo = [%d]\n", signo);
}

int main(int argc, char const *argv[])
{

    signal(SIGCHLD, sighandler);

    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork error");
        return -1;
    }
    else if (pid > 0)
    {
        //父进程
        printf("father:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        while (1)
        {
            sleep(1);
        }
    }
    else if (pid == 0)
    {
        //子进程
        printf("child:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        while (1)
        {
            sleep(1);
        }
    }

    return 0;
}
