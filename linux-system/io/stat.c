//stat函数测试：获取文件大小 文件属主和组 、文件类型
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
    //int stat(const char *pathname,struct stat *buf)
    struct stat st;
    stat(argv[1],&st);

    //文件属性
    printf("[%d],[%d],[%d]\n",st.st_size,st.st_uid,st.st_gid);

    //文件类型
    if((st.st_mode & S_IFMT) == S_IFREG)
    {
        printf("普通文件\n");
    }
    else if((st.st_mode & S_IFMT) == S_IFDIR)
    {
        printf("目录文件\n");
    }
    else if((st.st_mode & S_IFMT) == S_IFLNK)
    {
        printf("链接文件\n");
    }

    if(S_ISREG(st.st_mode))
    {
        printf("普通文件\n");
    }
    else if(S_ISDIR(st.st_mode))
    {     
        printf("目录文件\n");
    }
    else if(S_ISLNK(st.st_mode))
    {  
        printf("链接文件\n");
    }

	return 0;
}
