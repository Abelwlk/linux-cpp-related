//循环创建子线程
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

//线程执行函数
void *mythread(void *arg)
{
    printf("child thread, pid==[%d], id==[%ld],rank = %d\n", getpid(), pthread_self(), *(int *)arg);
}

int main()
{
    int ret;
    int i = 0;
    int n = 5;
    int arr[5] = {0, 1, 2, 3, 4};
    pthread_t thread[5];
    for (i = 0; i < n; i++)
    {
        ret = pthread_create(&thread[i], NULL, mythread, &arr[i]);
        if (ret != 0)
        {
            printf("pthread_create error, [%s]\n", strerror(ret));
            return -1;
        }
    }

    printf("main thread, pid==[%d], id==[%ld]\n", getpid(), pthread_self());

    sleep(1);
    return 0;
}
