//创建子线程 传递参数
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

//线程执行函数
void *mythread(void *arg)
{
    int p = *(int *)arg;
    printf("%d\n", p);
    printf("child thread, pid==[%d], id==[%ld]\n", getpid(), pthread_self());
}

int main()
{
    int n = 99;
    //创建子线程
    pthread_t thread;
    int ret = pthread_create(&thread, NULL, mythread, &n);
    if (ret != 0)
    {
        printf("pthread_create error, [%s]\n", strerror(ret));
        return -1;
    }
    printf("main thread, pid==[%d], id==[%ld]\n", getpid(), pthread_self());

    sleep(1);
    return 0;
}
