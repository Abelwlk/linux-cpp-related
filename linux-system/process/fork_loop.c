#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    int i = 0;
    for (i = 0; i < 3; i++)
    {
        //创建子进程
        //pid_t fork(void)
        pid_t pid = fork();
        if (pid < 0)
        {
            perror("fork error");
            return -1;
        }
        else if (pid > 0)
        {
            //父进程
            printf("father:pid=[%d],fpid=[%d]\n", getpid(), getppid());
            //sleep(1);
        }
        else if (pid == 0)
        {
            //子进程
            printf("child:pid=[%d],fpid=[%d]\n", getpid(), getppid());
            //禁止子进程再创建孙子进程
            break;
        }
    }

    sleep(10);

    return 0;
}