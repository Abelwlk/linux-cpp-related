#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

int g_var = 99;

int main(int argc, char const *argv[])
{
    //创建子进程
    //pid_t fork(void)
    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork error");
        return -1;
    }
    else if (pid > 0)
    {
        //父进程
        printf("father:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        //sleep(1);
        //g_var++;
        printf("father:g_var=%p\n", &g_var);
    }
    else if (pid == 0)
    {
        //让子进程后执行
        sleep(1);
        //子进程
        printf("child:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        printf("child:g_var=%d\n", g_var);
        printf("child:g_var=%p\n", &g_var);
    }

    return 0;
}