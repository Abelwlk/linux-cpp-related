#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
    //创建子进程
    //pid_t fork(void)
    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork error");
        return -1;
    }
    else if (pid > 0)
    {
        //父进程
        printf("father:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        int status;

        while (1)
        {
            //pid_t wpid = waitpid(-1, &status, 0);//-1表示等待任意子进程，0表示阻塞
            pid_t wpid = waitpid(-1, &status, WNOHANG); //WNOHANG表示不阻塞
            printf("wpid=[%d]\n", wpid);
            if (wpid > 0)
            {
                if (WIFEXITED(status)) //正常退出
                {
                    printf("child normal exit,status = [%d]\n", WEXITSTATUS(status));
                }
                else if (WIFSIGNALED(status)) //被信号杀死
                {
                    printf("child killed by signal,signo = [%d]\n", WTERMSIG(status));
                }
            }
            else if (wpid == 0) //子进程还活着
            {
                printf("child is living ,wpid=[%d]\n", wpid);
            }
            else if (wpid == -1) //没有子进程
            {
                printf("no child is living,wpid=[%d]\n", wpid);
                break;
            }
        }
    }
    else if (pid == 0)
    {
        //子进程
        printf("child:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        sleep(1);
        return 9;
    }

    return 0;
}