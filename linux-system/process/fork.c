#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    // printf("before fork:pid=[%d]\n", getpid());

    //创建子进程
    //pid_t fork(void)
    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork error");
        return -1;
    }
    else if (pid > 0)
    {
        //父进程
        printf("father:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        //sleep(1);
    }
    else if (pid == 0)
    {
        //子进程
        // printf("child:pid=[%d],fpid=[%d]\n", getpid(), getppid());
        // execl("/usr/bin/ls", "ls", "-l", NULL);
        // execl("./test", "test", "hello", "world", NULL);
        // execlp("ls", "ls", "-l", NULL);
        execlp("./test", "test", "hello", "world", NULL);
        perror("execl error");
    }

    // printf("after fork:pid=[%d]\n", getpid());

    return 0;
}