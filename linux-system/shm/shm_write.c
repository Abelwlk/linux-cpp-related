#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>

int main(int argc, char const *argv[])
{
    //创建共享内存
    int shmid = shmget(0x12345678, 1024, IPC_CREAT | IPC_EXCL | 0755);
    if (shmid < 0)
    {
        if (errno == EEXIST)
        {
            printf("shm already exist!\n");
            shmid = shmget(0x12345678, 0, 0);
        }
        else
        {
            return -1;
        }
    }
    printf("shmget success,shmid=[%d]\n", shmid);

    //关联共享内存
    void *pAddr = shmat(shmid, NULL, 0);
    if (pAddr == (void *)-1)
    {
        return -1;
    }
    printf("pAddr=[%p]\n", pAddr);

    //操作共享内存
    memcpy(pAddr, "hello world", strlen("hello world"));

    getchar();

    //断开与共享内存的关联
    shmdt(pAddr);

    getchar();

    //删除共享内存
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
