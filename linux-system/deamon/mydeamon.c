#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>

void myfunc(int signo)
{
    //打开文件
    int fd = open("mydeamon.log", O_RDWR | O_CREAT | O_APPEND, 0755);
    if (fd < 0)
    {
        return;
    }
    //获取当前系统时间
    time_t t;
    time(&t);
    char *p = ctime(&t);

    //将时间写入文件
    write(fd, p, strlen(p));
    close(fd);

    return;
}

int main(int argc, char const *argv[])
{
    //父进程fork子进程，然后父进程退出
    pid_t pid = fork();
    if (pid < 0 || pid > 0)
    {
        exit(1);
    }
    //子进程调用setsid函数创建会话
    setsid();

    //改变当前工作目录
    chdir("/home/abel/log");

    //改变文件掩码
    umask(0000);

    //关闭标准输入输出错误输出文件描述符
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //核心操作
    //注册信号处理函数
    struct sigaction act;
    act.sa_handler = myfunc;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGALRM, &act, NULL);

    //设置时钟，定时器
    struct itimerval tm;
    //执行间隔
    tm.it_interval.tv_sec = 2;
    tm.it_interval.tv_usec = 0;
    //首次执行
    tm.it_value.tv_sec = 3;
    tm.it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, &tm, NULL);

    printf("hello world!\n");

    while (1)
    {
        sleep(1);
    }

    return 0;
}
