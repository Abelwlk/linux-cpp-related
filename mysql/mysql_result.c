#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "mysql/mysql.h"

int main(int argc, char const *argv[])
{
    //初始化
    MYSQL *mysql = mysql_init(NULL);
    if (mysql == NULL)
    {
        printf("mysql init error\n");
        return -1;
    }
    printf("mysql init ok!\n");

    //连接mysql数据库
    MYSQL *conn = mysql_real_connect(mysql, NULL, "root", "root", "test", 0, NULL, 0);
    if (conn == NULL)
    {
        printf("mysql_real_connect error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    printf("mysql connect ok!,[%p],[%p]\n", mysql, conn);
    mysql_query(conn, "SET NAMES UTF8");

    //执行sql语句
    char sql[255] = "select * from user";
    int ret = mysql_query(conn, sql);
    if (ret != 0)
    {
        printf("mysql_query error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    printf("mysql_query ok!\n");

    //从mysql获取列数(在结果集之前)
    unsigned int num1 = mysql_field_count(conn);

    //获取结果集
    MYSQL_RES *result = mysql_store_result(conn);
    if (result == NULL)
    {
        printf("mysql_store_result error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    printf("mysql_store_result ok!\n");

    //从结果集获取列数(在结果集之后)
    unsigned int num2 = mysql_num_fields(result);

    //获取列名
    MYSQL_FIELD *fields = mysql_fetch_fields(result);
    if (fields == NULL)
    {
        printf("mysql_fetch_fields error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    printf("mysql_fetch_fields ok!\n");

    int i = 0;

    for (i = 0; i < num1; i++)
    {
        printf("%s\t", fields[i].name);
    }
    printf("\n");

    //获取每一行记录
    MYSQL_ROW row;
    while ((row = mysql_fetch_row(result)))
    {
        // printf("id=%s,name=%s,age=%s,sex=%s\n", row[0], row[1], row[2], row[3]);
        for (i = 0; i < num2; i++)
        {
            printf("%s\t", row[i]);
        }
        printf("\n");
    }

    //释放结果集
    mysql_free_result(result);

    //关闭连接
    mysql_close(conn);

    return 0;
}