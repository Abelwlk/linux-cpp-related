#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "mysql/mysql.h"

int main(int argc, char const *argv[])
{
    MYSQL *mysql = mysql_init(NULL);
    if (mysql == NULL)
    {
        printf("mysql init error\n");
        return -1;
    }
    printf("mysql init ok!\n");
    return 0;
}
