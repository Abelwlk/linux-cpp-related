#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "mysql/mysql.h"

int main(int argc, char const *argv[])
{
    //初始化
    MYSQL *mysql = mysql_init(NULL);
    if (mysql == NULL)
    {
        printf("mysql init error\n");
        return -1;
    }
    printf("mysql init ok!\n");

    //连接mysql数据库
    MYSQL *conn = mysql_real_connect(mysql, NULL, "root", "root", "test", 0, NULL, 0);
    if (conn == NULL)
    {
        printf("mysql_real_connect error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    printf("mysql connect ok!,[%p],[%p]\n", mysql, conn);
    mysql_query(conn, "SET NAMES UTF8");

    //执行sql语句
    char sql[255] = "insert into user(name,age,sex) values('wlk',18,'male')";
    int ret = mysql_query(conn, sql);
    if (ret != 0)
    {
        printf("mysql_query error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    printf("mysql_query ok!\n");

    //关闭连接
    mysql_close(conn);

    return 0;
}