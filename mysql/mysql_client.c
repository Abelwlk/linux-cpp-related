#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "mysql/mysql.h"

int main(int argc, char const *argv[])
{
    //初始化
    MYSQL *mysql = mysql_init(NULL);
    if (mysql == NULL)
    {
        printf("mysql init error\n");
        return -1;
    }

    //连接mysql数据库
    MYSQL *conn = mysql_real_connect(mysql, NULL, "root", "root", "test", 0, NULL, 0);
    if (conn == NULL)
    {
        printf("mysql_real_connect error,[%s]\n", mysql_error(mysql));
        return -1;
    }
    // 1.设置本次连接的字符集
    // mysql_query(conn, "SET NAMES UTF8");

    // 2.设置本次连接的字符集
    if (!mysql_set_character_set(conn, "UTF8"))
    {
        printf("New client character set: %s\n", mysql_character_set_name(conn));
    }

    int i;
    int n;
    int ret;
    int num;
    char *p;
    char buf[1024];
    MYSQL_RES *results;
    MYSQL_FIELD *fields;
    MYSQL_ROW row;

    //进入循环等待用户输入操作
    while (1)
    {
        //打印提示符
        write(STDOUT_FILENO, "mysql> ", strlen("mysql> "));

        //读取用户输入
        memset(buf, 0x00, sizeof(buf));
        read(STDIN_FILENO, buf, sizeof(buf));

        //去掉末尾[;]
        p = strrchr(buf, ';');
        if (p != NULL)
        {
            *p = '\0';
        }

        //去掉回车
        if (buf[0] == '\n')
        {
            continue;
        }

        //去掉前面的空格
        for (i = 0; i < strlen(buf); i++)
        {
            if (buf[i] != ' ')
            {
                break;
            }
        }
        n = strlen(buf);
        //加1表示多拷贝一个\0
        memmove(buf, buf + i, n - i + 1);

        printf("%s\n", buf);

        //如果输入的是退出： exit EXIT quit QUIT
        if (strncasecmp(buf, "exit", 4) == 0 || strncasecmp(buf, "quit", 4) == 0)
        {
            //关闭连接
            mysql_close(conn);
            write(STDOUT_FILENO, "Bye\n", strlen("Bye\n"));
            exit(0);
        }

        //执行sql
        ret = mysql_query(conn, buf);
        if (ret != 0)
        {
            printf("%s\n", mysql_error(conn));
            continue;
        }

        //不是select操作
        if (strncasecmp(buf, "select", 6) != 0)
        {
            printf("Query OK, %lld row affected\n", mysql_affected_rows(conn));
            continue;
        }

        //是select操作
        //获取结果集
        results = mysql_store_result(conn);
        if (results == NULL)
        {
            printf("%s\n", mysql_error(conn));
            continue;
        }

        //从结果集获取列数(在结果集之后)
        num = mysql_num_fields(results);
        //获取列名并打印
        fields = mysql_fetch_fields(results);
        if (fields == NULL)
        {
            printf("%s\n", mysql_error(mysql));
            //释放结果集
            mysql_free_result(results);
            continue;
        }
        //打印表头
        for (i = 0; i < num; i++)
        {
            printf("%s\t", fields[i].name);
        }
        printf("\n");

        //循环获取每一行记录
        while ((row = mysql_fetch_row(results)))
        {
            for (i = 0; i < num; i++)
            {
                printf("%s\t", row[i]);
            }
            printf("\n");
        }
        //释放结果集
        mysql_free_result(results);
    }

    //关闭连接
    mysql_close(conn);

    return 0;
}