#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <ctype.h>
#include <event2/event.h>

struct event *connev = NULL;

void readcb(evutil_socket_t fd, short events, void *arg)
{
    int n;
    char buf[1024];
    memset(buf, 0x00, sizeof(buf));
    n = read(fd, buf, sizeof(buf));
    if (n <= 0)
    {
        close(fd);
        //将通信文件描述符对应的事件从event_base地基上删除
        event_del(connev);
        printf("readcb==close\n");
    }
    else
    {
        for (int i = 0; i < n; i++)
        {
            buf[i] = toupper(buf[i]);
        }

        write(fd, buf, n);
    }
    printf("readcb\n");
}

void *conncb(evutil_socket_t fd, short events, void *arg)
{
    struct event_base *base = (struct event_base *)arg;

    //接受新的客户端连接
    int cfd = accept(fd, NULL, NULL);
    if (cfd > 0)
    {
        //创建通信文件描述符对应的事件
        connev = event_new(base, cfd, EV_READ | EV_PERSIST, readcb, NULL);
        if (connev == NULL)
        {
            event_base_loopexit(base, NULL);
        }

        //将通信文件描述符对应的事件上event_base
        event_add(connev, NULL);

        printf("conncb\n");
    }
}

int main(int argc, char const *argv[])
{
    //创建socket
    int lfd = socket(AF_INET, SOCK_STREAM, 0);

    //设置端口复用
    int opt = 1;
    setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

    //绑定
    struct sockaddr_in serv;
    bzero(&serv, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_port = htons(8080);
    serv.sin_addr.s_addr = htonl(INADDR_ANY);
    int ret = bind(lfd, (struct sockaddr *)&serv, sizeof(serv));
    if (ret < 0)
    {
        perror("bind error");
        return -1;
    }
    //设置监听
    listen(lfd, 128);

    //创建地基
    struct event_base *base = event_base_new();
    if (base == NULL)
    {
        printf("event_base_new error\n");
        return -1;
    }

    //创建监听文件描述符对应的事件
    struct event *ev = event_new(base, lfd, EV_READ | EV_PERSIST, conncb, base);
    if (ev == NULL)
    {
        printf("event_new error\n");
        return -1;
    }

    //将新的事件节点上base地基
    event_add(ev, NULL);

    //进入事件循环等待
    event_base_dispatch(base);

    //释放资源
    event_free(ev);
    event_base_free(base);
    close(lfd);

    return 0;
}
