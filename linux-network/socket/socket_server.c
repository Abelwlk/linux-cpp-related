#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <ctype.h>

int main(int argc, char const *argv[])
{
    //创建socket int socket(int domain, int type, int protocol);
    int sfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sfd < 0)
    {
        perror("socket error");
        return -1;
    }

    //绑定 int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
    struct sockaddr_in serv;
    bzero(&serv, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_port = htons(8080);
    serv.sin_addr.s_addr = htonl(INADDR_ANY); //使用本机任意可用ip
    int ret = bind(sfd, (struct sockaddr *)&serv, sizeof(serv));
    if (ret < 0)
    {
        perror("bind error");
        return -1;
    }

    //监听 int listen(int sockfd, int backlog);
    listen(sfd, 128);

    //int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
    struct sockaddr_in client;
    socklen_t len = sizeof(client);
    //int cfd = accept(sfd, NULL, NULL);
    int cfd = accept(sfd, (struct sockaddr *)&client, &len);
    char ip[16];
    memset(ip, 0x00, sizeof(ip));
    printf("client:ip==[%s],port==[%d]\n", inet_ntop(AF_INET, &client.sin_addr.s_addr, ip, sizeof(ip)), ntohs(client.sin_port));

    printf("sfd:[%d],cfd:[%d]\n", sfd, cfd);

    int i = 0;
    int n = 0;
    char buf[1024];

    while (1)
    {
        //读数据
        memset(buf, 0x00, sizeof(buf));
        n = read(cfd, buf, sizeof(buf));
        if (n <= 0)
        {
            printf("read error or client closed,n==[%d]\n", n);
            break;
        }
        printf("n==[%d],buf==[%s]\n", n, buf);
        for (i = 0; i < n; i++)
        {
            buf[i] = toupper(buf[i]);
        }

        //写数据
        write(cfd, buf, n);
    }

    //关闭 监听文件描述符 和 通信文件描述符
    close(cfd);
    close(sfd);

    return 0;
}