#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "wrap.h"
#include <ctype.h>
#include <signal.h>
#include <sys/wait.h>

void waitchild(int signo)
{
    pid_t wpid;

    //回收子进程
    while (1)
    {
        wpid = waitpid(-1, NULL, WNOHANG);
        if (wpid > 0)
        {
            printf("child is quit, wpid==[%d]\n", wpid);
        }
        else if (wpid == 0)
        {
            printf("child is living, wpid==[%d]\n", wpid);
            break;
        }
        else if (wpid == -1)
        {
            printf("no child is living, wpid==[%d]\n", wpid);
            break;
        }
    }
}

int main(int argc, char const *argv[])
{
    //创建socket
    int lfd = Socket(AF_INET, SOCK_STREAM, 0);

    //设置端口复用
    int opt = 1;
    setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

    //绑定
    struct sockaddr_in serv;
    bzero(&serv, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_port = htons(8080);
    serv.sin_addr.s_addr = htonl(INADDR_ANY);
    Bind(lfd, (struct sockaddr *)&serv, sizeof(serv));
    //设置监听
    Listen(lfd, 128);

    pid_t pid;
    int cfd;
    int i = 0;
    char sIP[16];
    socklen_t len;
    struct sockaddr_in client;
    while (1)
    {
        //接受新的连接
        len = sizeof(client);
        memset(sIP, 0x00, sizeof(sIP));
        cfd = Accept(lfd, (struct sockaddr *)&client, &len);
        printf("client==[%s],prot==[%d]\n", inet_ntop(AF_INET, &client.sin_addr.s_addr, sIP, sizeof(sIP)), ntohs(client.sin_port));

        //接受新的连接创建子进程，让子进程收发操作
        pid = fork();
        if (pid < 0)
        {
            perror("fork error");
            exit(-1);
        }
        else if (pid > 0)
        {
            //关闭通信文件描述符
            close(cfd);
            signal(SIGCHLD, waitchild);
        }
        else if (pid == 0)
        {
            //关闭监听文件描述符
            close(lfd);

            int n;
            char buf[1024];
            while (1)
            {
                memset(buf, 0x00, sizeof(buf));
                //读数据
                n = Read(cfd, buf, sizeof(buf));
                if (n <= 0)
                {
                    printf("read fail or client close,n==[%d]\n", n);
                    break;
                }
                printf("buf==[%s]\n", buf);
                printf("client==[%s],prot==[%d]\n", inet_ntop(AF_INET, &client.sin_addr.s_addr, sIP, sizeof(sIP)), ntohs(client.sin_port));

                //写数据
                for (i = 0; i < n; i++)
                {
                    buf[i] = toupper(buf[i]);
                }
                Write(cfd, buf, n);
            }
            //关闭通信文件描述符
            close(cfd);
            //关闭进程
            exit(0);
        }
    }

    //关闭监听文件描述符
    close(lfd);

    return 0;
}
