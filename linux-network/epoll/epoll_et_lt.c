#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <ctype.h>
#include <fcntl.h>
#include "../tcp/wrap.h"

int main(int argc, char const *argv[])
{
    int ret;
    int n;
    int i;
    int k;
    int nready;
    int lfd;
    int cfd;
    int sockfd;
    char buf[1024];
    socklen_t socklen;
    struct sockaddr_in svraddr;
    struct epoll_event ev;
    struct epoll_event events[1024];

    //创建socket
    lfd = Socket(AF_INET, SOCK_STREAM, 0);

    //设置端口复用
    int opt = 1;
    setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

    //绑定--将lfd 和 IP PORT绑定
    struct sockaddr_in serv;
    bzero(&serv, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_port = htons(8080);
    serv.sin_addr.s_addr = htonl(INADDR_ANY);
    Bind(lfd, (struct sockaddr *)&serv, sizeof(serv));

    //监听
    Listen(lfd, 128);

    //创建一棵epoll树
    int epfd = epoll_create(1024);
    if (epfd < 0)
    {
        perror("create epoll error");
        return -1;
    }

    //将lfd上epoll树
    ev.data.fd = lfd;
    ev.events = EPOLLIN;
    epoll_ctl(epfd, EPOLL_CTL_ADD, lfd, &ev);

    while (1)
    {
        nready = epoll_wait(epfd, events, 1024, -1);
        if (nready < 0)
        {
            perror("epoll_wait error");
            if (errno == EINTR)
            {
                continue;
            }
            break;
        }

        for (i = 0; i < nready; i++)
        {
            //有客户端连接请求
            sockfd = events[i].data.fd;
            if (sockfd == lfd)
            {
                cfd = Accept(lfd, NULL, NULL);
                //新的cfd上树
                ev.data.fd = cfd;
                //et模式
                ev.events = EPOLLIN | EPOLLET;
                epoll_ctl(epfd, EPOLL_CTL_ADD, cfd, &ev);
                //将cfd设置为非阻塞
                int flag = fcntl(cfd, F_GETFL);
                flag = flag | O_NONBLOCK;
                fcntl(cfd, F_SETFL, flag);
                continue;
            }

            //客户端发送数据
            memset(buf, 0x00, sizeof(buf));
            //n = recv(sockfd, buf, sizeof(buf), 0);
            while (1)
            {
                n = Read(sockfd, buf, 2);
                printf("n==[%d],buf==[%s]\n", n, buf);
                //读完数据的情况
                if (n == -1)
                {
                    break;
                }
                //对方关闭连接或者异常
                if (n == 0 | n < 0 && n != -1)
                {
                    close(sockfd);
                    //将事件节点从epoll树删除
                    epoll_ctl(epfd, EPOLL_CTL_DEL, sockfd, NULL);
                }
                else //读数据
                {
                    printf("n==[%d],buf==[%s]\n", n, buf);
                    for (k = 0; k < n; k++)
                    {
                        buf[k] = toupper(buf[k]);
                    }
                    Write(sockfd, buf, n);
                }
            }
        }
    }

    close(epfd);
    close(lfd);
    return 0;
}
