#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <ctype.h>
#include <errno.h>
#include "../tcp/wrap.h"

int main(int argc, char const *argv[])
{
    //创建socket
    int lfd = Socket(AF_INET, SOCK_STREAM, 0);

    //设置端口复用
    int opt = 1;
    setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));

    //绑定
    struct sockaddr_in serv;
    bzero(&serv, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_port = htons(8080);
    serv.sin_addr.s_addr = htonl(INADDR_ANY);
    Bind(lfd, (struct sockaddr *)&serv, sizeof(serv));
    //设置监听
    Listen(lfd, 128);

    //定义fd_set类型变量
    fd_set readfds;
    fd_set tmpfds;

    //清空readfds和tmpfds
    FD_ZERO(&readfds);
    FD_ZERO(&tmpfds);

    //将lfd加入到readfds中，委托到内核监控
    FD_SET(lfd, &readfds);

    int nready;
    int maxfd = lfd;
    int cfd;
    int i, j;
    int sockfd;
    int n;
    char buf[1024];

    while (1)
    {
        tmpfds = readfds;
        //tmpfds是输入输出函数
        //传入:指的是告诉内核哪些文件描述符需要监控
        // 传出:指的是内核告诉应用程序哪些文件描述符发生了变化
        nready = select(maxfd + 1, &tmpfds, NULL, NULL, NULL);

        if (nready < 0)
        {
            //被信号中断
            if (errno == EINTR)
            {
                continue;
            }
            break;
        }

        //有客户端连接请求
        if (FD_ISSET(lfd, &tmpfds))
        {
            //接受新的客户端连接请求
            cfd = Accept(lfd, NULL, NULL);
            //将cfd加入到readfds中
            FD_SET(cfd, &readfds);

            //修改内核的监控范围
            if (maxfd < cfd)
            {
                maxfd = cfd;
            }

            if (--nready == 0)
            {
                continue;
            }
        }

        //有数据发来的情况
        for (i = lfd + 1; i <= maxfd; i++)
        {
            sockfd = i;
            //判断sockfd文件描述符是否有变化
            if (FD_ISSET(sockfd, &tmpfds))
            {
                //读数据
                memset(buf, 0x00, sizeof(buf));
                n = Read(sockfd, buf, sizeof(buf));
                if (n <= 0)
                {
                    //关闭连接
                    close(sockfd);
                    //将sockfd从readfds删除
                    FD_CLR(sockfd, &readfds);
                }
                else
                {
                    printf("n==[%d],buf==[%s]\n", n, buf);
                    for (j = 0; j < n; j++)
                    {
                        buf[j] = toupper(buf[j]);
                    }
                    Writen(sockfd, buf, n);
                }

                if (--nready == 0)
                {
                    break;
                }
            }
        }
    }

    close(lfd);

    return 0;
}